package ficha5grupoiii;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
/**
 *
 * @author helde
 */
public class ficha5grupoiii {
    
    public static void main(String[] args) {
/*
        int [][] Matriz = new int[6][6]; 
        int l = 55;
        int ll = l;

        Scanner entrada = new Scanner(System.in); 
        System.out.println("Matriz M[6][6]\n");
        
        Matriz[0][0]=ll;
        
            for(int linha=0 ; linha < 1 ; linha++){
                for(int coluna = 1; coluna < 6 ; coluna ++){
                    Matriz[linha][coluna]=coluna;
                }
            }
    
            for(int linha=1 ; linha < 6 ; linha++){
                for(int coluna = 0; coluna < 1 ; coluna ++){
                    Matriz[linha][coluna]=linha;
                }
            }
            
        
            for(int linha=1 ; linha < 6 ; linha++){
                for(int coluna = 1; coluna < 6 ; coluna ++){
                    System.out.printf("Insira o elemento M[%d][%d]: ",linha+1,coluna+1);
                    Matriz[linha][coluna]=entrada.nextInt();
                }
            }
            
            System.out.println("\nA Matriz ficou: \n");
            for(int linha=0 ; linha < 6 ; linha++){
                for(int coluna = 0; coluna < 6 ; coluna ++){
                    System.out.printf("\t %d \t",Matriz[linha][coluna]);
                }
                System.out.println();
            } 
          
    */
    Scanner ler = new Scanner(System.in);
    System.out.printf("Informe o nome de arquivo texto:\n");
    String nome = ler.nextLine();
    
    System.out.printf("\nConteúdo do arquivo texto:\n");
    try {
      FileReader arq = new FileReader(nome);
      BufferedReader lerArq = new BufferedReader(arq);
 
      String linha = lerArq.readLine(); // lê a primeira linha
// a variável "linha" recebe o valor "null" quando o processo
// de repetição atingir o final do arquivo texto
      while (linha != null) {
        System.out.printf("%s\n", linha);
 
        linha = lerArq.readLine(); // lê da segunda até a última linha
      }
 
      arq.close();
    } catch (IOException e) {
        System.err.printf("Erro na abertura do arquivo: %s.\n",
          e.getMessage());
    }
 
    System.out.println();
          
    }
  
    
    
}
